unit Tenuot_Atarot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, FileCtrl, ExtCtrls, Mask, ToolEdit,
  Db, Grids, DBGrids, DBCtrls;

type
  TF_Tenuot_Atarot = class(TForm)
    BitBtn1: TBitBtn;
    Btn_Klita: TBitBtn;
    Klita_Dir: TFilenameEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edt_Rehev: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Edt_Nehag: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Edt_Shana: TEdit;
    Label5: TLabel;
    Edt_Month: TEdit;
    Edt_shem_nehag: TEdit;
    Bbn_SaveIni: TBitBtn;
    BitBtn2: TBitBtn;
    procedure Btn_KlitaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);

    Procedure Bedikat_Netunim;
    procedure SpeedButton2Click(Sender: TObject);
    procedure Edt_NehagChange(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure ReadIniFile;
    procedure Bbn_SaveIniClick(Sender: TObject);
    procedure ErrorLine(Mis_Shura,Sug_Takala,Mis_Teuda,Nehag,Rehev : String);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);

    procedure PutMerakezDataInField(MerakezName:String;TheField :TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Call_Tenuot_Atarot(LDbAliasName,LDbUserName,LDbPassword,LIniFile:Pchar);

var
  F_Tenuot_Atarot: TF_Tenuot_Atarot;

  PrintText :System.Text;   {���� ������}

  Sw_Stop : boolean; // ����� ������
Type
    FileZ = Record
      Shura : Char;
    end;
Var
    F: File Of Filez;

    Sw_Takin : Boolean;

    Kod_Nehag,Mis_Rishui,File_Name,Kod_Sapak : String;
//������� ������ ������ ����� ��� �� �����
Function ConvertToRishuy(InStr :String) :String;
Function DelLeadingZeroFromRishuy (SrcStr:String):String;
  function DosToWin(InStr:string):string;
  function LeftToRight(InStr:string):string;

// ���� ���� ������� ������ �"� �� �����
Function CountFieldsInSepString(TheString:String;TheSeparator :Char):LongInt;

//������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                    FieldNum :LongInt): String;

implementation

uses F_Mesage, IniFiles, Dm_Message;
{$R *.DFM}

var
  IniFile: TIniFile;

//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.Btn_KlitaClick(Sender: TObject);
var
  Total_Records ,Mis_Shura: integer;
  PrintStr ,HStr,AliasDirectory: String;

  Taarich_Temp,Taarich_Bitzua,Shaa_Temp,Shaa_Bitzua : string;
//  PrintText :System.Text;   {���� ������}
  SonolFile :TextFile;      {���� ����}
  LastAzmnValue: Integer; // ����� ��' ����� �����
  LastHavaraValue: Integer; // ��' ����� �����

  F_Mesage : TF_Message;    {���� ����� ���� ������}

  Var_Sug_rikuz : String;
begin
  if MessageDlg('? �� �� ������ ���� ������� ��� ������', mtConfirmation,[mbYes, mbNo], 0) = mrNo then
     exit;

  Bedikat_Netunim;   //    ����� ������

  if Not Sw_takin then
     exit;
//--------------------------
  Sw_Stop := false;
//-----------------
 DMod_Message.Tbl_AtmAzmn.Open;
  DMod_Message.Tbl_AtmShib.Open;
//-----------------  ��' ������� �����
  AssignFile (F, (Klita_Dir.Text));
  Reset (F);
  Total_Records := Trunc (FileSize (F)/ 93);
  CloseFile (F);

//---------------------------------
  AssignFile (SonolFile, (Klita_Dir.Text));
  Reset (SonolFile);
  AssignFile (PrintText, (ExtractFilePath(Application.ExeName) + 'Shagui.Txt'));
  Rewrite (PrintText);

  F_Mesage := TF_Message.Create (Self);
  F_Mesage.Show;
  F_Mesage.Caption := '����� ���� ����� �����';

//  showmessage ('1');
  Mis_Shura := 0;

       try
//----------------------- ��' ����� ����� ������

//DMod_Message.Tbl_AtmAzmn.Last  ;
//ShowMessage(DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnNo').asstring);
  With DMod_Message.Qry_Inter do
     begin
       Close;
       Sql.Clear;
      Sql.Add ('Select Max(AzmnNo) As MaxNum From AtmAzmn');
     //  Sql.Add ('Select * From lakoach');
    //   ExecSQL
       Open;
     end;
       Except
//        ShowMessage ('���� ���� ���� ������� '+IntToStr (Mis_Shura));
        on e:Exception do
        ShowMessage (E.Message);
      
    end;

 //   LastAzmnValue :=            DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnNo').AsInteger  ;

  LastAzmnValue := DMod_Message.Qry_Inter.FieldByName('MaxNum').AsInteger;

//----------------------- ��' ����� �����
(*  With DMod_Message.Qry_Inter do
     begin
       Close;
       Sql.Clear;
       Sql.Add ('Select Max(MaklidName) As MaxNum From AtmAzmn');
       Open;
     end;
  LastHavaraValue := DMod_Message.Qry_Inter.FieldByName('MaxNum').AsInteger+1;
  *)
//  showmessage ('3');
//-----------------------------
  while (Not Eof(SonolFile)) AND
        (Not sw_Stop)  do
    begin
      Inc (Mis_Shura);
      F_Mesage.Total_Records.Caption := IntToStr(Total_Records);
      F_Mesage.Mis_Shura.Caption := IntToStr(Mis_Shura);
      F_Mesage.ProgressBar.Position := Trunc ((Mis_Shura / Total_Records)*100);

      Application.ProcessMessages;

      Readln (SonolFile,HStr);
      PrintStr := '';           {���� ���� �������}
//---------------------- ���� �������
      try
(*      With DMod_Message.Qry_Inter do
         begin
           Close;
           Sql.Clear;
           Sql.Add ('Select Max(AzmnNo) As MaxNum From AtmAzmn');
           Open;
         end;
      LastAzmnValue := DMod_Message.Qry_Inter.FieldByName('MaxNum').AsInteger;
*)
//      showmessage ('4');
      LastAzmnValue := LastAzmnValue +1;
      DMod_Message.Tbl_AtmAzmn.Insert;

//      showmessage ('5');
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnNo').AsInteger:=
           LastAzmnValue;
//--------------------- ����� ������ ������
      Taarich_Temp:=Copy(HStr,80,8);
      Taarich_Bitzua:= '';
      Insert (Copy(Taarich_Temp,1,2),Taarich_Bitzua,1);
      Insert ('/',Taarich_Bitzua,3);
      Insert (Copy(Taarich_Temp,4,2),Taarich_Bitzua,4);
      Insert ('/',Taarich_Bitzua,6);
      if (StrToInt(Copy(Taarich_Temp,7,2)) > 30) then  // ����� ���� 2000
         Insert ('19'+Copy(Taarich_Temp,7,2),Taarich_Bitzua,7)
      else
         Insert (Copy(Taarich_Temp,7,2),Taarich_Bitzua,7);
                                 {���}
      Shaa_Temp:=Copy(HStr,50,4);
      Shaa_Bitzua:= '';
      if Copy(Shaa_Temp,1,2) = '24' then
         Insert ('00',Shaa_Bitzua,1)
      else
         Insert (Copy(Shaa_Temp,1,2),Shaa_Bitzua,1);
      Insert (':',Shaa_Bitzua,3);
      Insert (Copy(Shaa_Temp,3,2),Shaa_Bitzua,4);

      try
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime :=
                                        StrToDate(Taarich_Bitzua) +
                                        StrToTime(Shaa_Bitzua);
      except
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime:=
           StrToDateTime(Copy(HStr,80,8));

      end;
//--------------------------------------------------
      DMod_Message.Tbl_AtmAzmn.FieldByname ('InvLakNo').AsInteger:=
           StrToInt(Copy(HStr,91,3));
      DMod_Message.Tbl_AtmAzmn.FieldByname ('FromDate').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('ToDate').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('ToDate').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;
                            {���� ����� }
      DMod_Message.Tbl_AtmAzmn.FieldByname ('TotalPrice1').AsFloat:=0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('TotalPrice2').AsFloat:=0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('TotalPrice3').AsFloat:=0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Quntity1').AsFloat:=1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnQuntity').AsInteger:= 1;

      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnType').AsInteger:= 2;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Sunday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Monday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Tuesday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Wednesday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Thursday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Friday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Saturday').AsInteger:= 1;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('MaklidName').AsString:= '1'{IntToStr(LastHavaraValue)};

      DMod_Message.Tbl_AtmAzmn.FieldByname ('GetLakNo').AsInteger:=
         DMod_Message.Tbl_AtmAzmn.FieldByname ('InvLakNo').AsInteger;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Asmcta').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Tuda').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('TotalKm').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('TotalZikuim').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('OldAzmnType').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnCarKind').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnSugMetan').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('DirshNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('FirstAzmnNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnMaslulCode1').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnMaslulCode2').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnMaslulCode3').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnMaslulCode4').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnCarNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDriverNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('CodeMhiron').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Mitzar').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Rshimon').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Nefh').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Damaged').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('HovalaKind').AsInteger:= 0;
      DMod_Message.Tbl_AtmAzmn.FieldByname ('Mishkal').AsInteger:= 0;

      Except
//        ShowMessage ('���� ���� ���� ������� '+IntToStr (Mis_Shura));
        on e:Exception do
        ShowMessage (E.Message);
      end;
      try
        DMod_Message.Tbl_AtmAzmn.Post;
      except
        on e:Exception do
        begin
        ShowMessage (E.Message);

        ErrorLine(IntToStr(Mis_Shura),'���� ������ ������',Copy(HStr,50,6),Copy(HStr,74,5),Copy(HStr,60,7));
        end;
      end;
//--------------------------- ���� ��������
      try
      DMod_Message.Tbl_AtmShib.Insert;

      DMod_Message.Tbl_AtmShib.FieldByname ('ShibAzmnNo').AsInteger:=
           LastAzmnValue{ +1};

      DMod_Message.Tbl_AtmShib.FieldByname ('ShibAzmnDate').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;

      DMod_Message.Tbl_AtmShib.FieldByname ('ShibKind').AsInteger:=1;
      DMod_Message.Tbl_AtmShib.FieldByname ('AzmnOk').AsInteger:=1;
//----------------------------- ���
//      DMod_Message.Tbl_AtmShib.FieldByname ('CarNum').AsString:=
//      ConvertToRishuy(Trim(DelLeadingZeroFromRishuy (Copy(HStr,11,7))));

      With DMod_Message.Qry_2 do
         begin
           Close;
           Sql.Clear;
           Sql.Add ('Select Mis_Rishui,Mis_Rehev From Rehev');
           Sql.Add ('Where Mis_Rehev = '+''''+Copy(HStr,60,7)+'''');
           Open;

           if FieldByName ('Mis_Rehev').AsString = Copy(HStr,60,7) then
              DMod_Message.Tbl_AtmShib.FieldByname ('CarNum').AsString:=
              FieldByName ('Mis_Rishui').AsString
           else
              begin
              DMod_Message.Tbl_AtmShib.FieldByname ('CarNum').AsString:=
              Edt_Rehev.Text;

              ErrorLine(IntToStr(Mis_Shura),'��� �� ����',Copy(HStr,50,6),Copy(HStr,74,5),Copy(HStr,60,7));
              end;
         end;
//-------------------------
      DMod_Message.Tbl_AtmShib.FieldByname ('UpdateRecordDate').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;;

      DMod_Message.Tbl_AtmShib.FieldByname ('ShibTuda').AsInteger:=
           StrToInt(Trim(Copy(HStr,54,4)));

      DMod_Message.Tbl_AtmShib.FieldByname ('ShibBeginTime').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;

      DMod_Message.Tbl_AtmShib.FieldByname ('ShibEndTime').AsDateTime:=
           DMod_Message.Tbl_AtmAzmn.FieldByname ('AzmnDate').AsDateTime;

      DMod_Message.Tbl_AtmShib.FieldByname ('LakNo1').AsInteger:=
           StrToInt(Trim(Copy(HStr,91,3)));
                              {����� ����}
      DMod_Message.Tbl_AtmShib.FieldByname ('YeMida1').AsInteger:= 1;
      DMod_Message.Tbl_AtmShib.FieldByname ('YeMida2').AsInteger:=
         DMod_Message.Tbl_AtmShib.FieldByname ('YeMida1').AsInteger;
      DMod_Message.Tbl_AtmShib.FieldByname ('YeMida3').AsInteger:=
         DMod_Message.Tbl_AtmShib.FieldByname ('YeMida1').AsInteger;
      DMod_Message.Tbl_AtmShib.FieldByname ('YeMida4').AsInteger:=
         DMod_Message.Tbl_AtmShib.FieldByname ('YeMida1').AsInteger;

      DMod_Message.Tbl_AtmShib.FieldByname ('MaklidName').AsString:= '1'{IntToStr(LastHavaraValue)};
      DMod_Message.Tbl_AtmShib.FieldByname ('YehusMonth').AsInteger:=
                                     StrToInt(Edt_Month.Text);
      DMod_Message.Tbl_AtmShib.FieldByname ('YehusYear').AsInteger:=
                                     StrToInt(Edt_Shana.Text);
//---------------------------- ���
      With DMod_Message.Qry_2 do
         begin
           Try
           Close;
           Sql.Clear;
           Sql.Add ('Select Kod_Nehag,Shem_nehag From Nehag');
           Sql.Add ('Where Kod_Nehag = '+IntToStr(StrToInt(Trim(Copy(HStr,76,3)))));
           Open;

           if FieldByName ('Kod_Nehag').AsString = IntToStr(StrToInt(Trim(Copy(HStr,76,3)))) then
              begin
              DMod_Message.Tbl_AtmShib.FieldByname ('DriverNo1').AsString:=
              FieldByName ('Kod_Nehag').AsString;
              DMod_Message.Tbl_AtmShib.FieldByname ('DriverName').AsString:=
              FieldByName ('Shem_Nehag').AsString;
              end
           else
              begin
              DMod_Message.Tbl_AtmShib.FieldByname ('DriverNo1').AsString:=
              Edt_Nehag.Text;

              ErrorLine(IntToStr(Mis_Shura),'��� �� ����',Copy(HStr,50,6),Copy(HStr,76,3),Copy(HStr,60,7));
              end;
           Except
              begin
              DMod_Message.Tbl_AtmShib.FieldByname ('DriverNo1').AsString:=
              Edt_Nehag.Text;

              ErrorLine(IntToStr(Mis_Shura),'��� �� ����',Copy(HStr,50,6),Copy(HStr,76,3),Copy(HStr,60,7));
              end;
           end;
         end;
        (*
      try
      DMod_Message.Tbl_AtmShib.FieldByname ('DriverNo1').AsInteger:=
           StrToInt(Copy(HStr,131,9));
      except
      DMod_Message.Tbl_AtmShib.FieldByname ('DriverNo1').AsString:=
           Edt_Nehag.Text;
      end;

      DMod_Message.Tbl_AtmShib.FieldByname ('DriverName').AsString:=
           Copy(HStr,141,15);
           *)
//----------------------------- ������ ���������
      DMod_Message.Tbl_AtmShib.FieldByname ('Quntity1').AsFloat:=1;
      DMod_Message.Tbl_AtmShib.FieldByname ('Quntity2').AsFloat:=
           DMod_Message.Tbl_AtmShib.FieldByname ('Quntity1').AsFloat;
      DMod_Message.Tbl_AtmShib.FieldByname ('Quntity3').AsFloat:=
           DMod_Message.Tbl_AtmShib.FieldByname ('Quntity1').AsFloat;
      DMod_Message.Tbl_AtmShib.FieldByname ('Quntity4').AsFloat:=
           DMod_Message.Tbl_AtmShib.FieldByname ('Quntity1').AsFloat;

      DMod_Message.Tbl_AtmShib.FieldByname ('PriceQuntity2').AsFloat:=0;

      DMod_Message.Tbl_AtmShib.FieldByname ('Price2').AsFloat:=0;
//-------------------------- �. ���� / ��' ����
(*
      DMod_Message.Tbl_AtmShib.FieldByname ('SugMetan').AsInteger:=
           StrToInt(Copy(HStr,95,10));
           *)
      DMod_Message.Tbl_AtmShib.FieldByname ('SugMetan').AsInteger:= StrToInt(Copy(HStr,48,1));
//--------------------------- ����� / ��� ����
      DMod_Message.Tbl_AtmShib.FieldByname ('MaslulCode1').AsInteger:=
           StrToInt(Copy(HStr,32,4));
      DMod_Message.Tbl_AtmShib.FieldByname ('Maslul1').AsString:=
           LeftToRight(DosToWin(Copy(HStr,11,20)));

//      DMod_Message.Tbl_AtmShib.FieldByname ('YehusYear').AsInteger := StrToInt(Edt_Shana.text);
//      DMod_Message.Tbl_AtmShib.FieldByname ('YehusMonth').AsInteger := StrToInt(Edt_Month.text);

      DMod_Message.Tbl_AtmShib.FieldByname ('HovalaKind').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('PriceKind').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('ShibTotalKm').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('LakNo2').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('LakNo3').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('PriceIsGlobal').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('Price1').AsFloat:=0;
      DMod_Message.Tbl_AtmShib.FieldByname ('Price3').AsFloat:=0;
      DMod_Message.Tbl_AtmShib.FieldByname ('Price4').AsFloat:=0;
      DMod_Message.Tbl_AtmShib.FieldByname ('PriceQuntity1').AsFloat:=0;
      DMod_Message.Tbl_AtmShib.FieldByname ('PriceQuntity3').AsFloat:=0;
      DMod_Message.Tbl_AtmShib.FieldByname ('PriceQuntity4').AsFloat:=0;
      DMod_Message.Tbl_AtmShib.FieldByname ('DriverNo2').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('CarNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('LakCodeBizua').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('LakHesbonit').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('DrishNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('DriverCodeBizua').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('DriverZikuiNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('CarCodeBizua').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('CarZikuiNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('Nefh').AsFloat:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('TrvlNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('StarMetanNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('GpsStatus').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('GpsNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('ShibCarKind').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('TnuaOk').AsInteger:= 1;
      DMod_Message.Tbl_AtmShib.FieldByname ('HasbonitOk').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('FirstSpido').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('TudaKind').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('NegrrNo').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('ShibAsmcta').AsInteger:= 0;
      DMod_Message.Tbl_AtmShib.FieldByname ('QuntMusa').AsFloat:= 0;
//------------------------- ����� ���� �� �����
       With DMod_Message.Qry_2 do
           begin
             Close;
             Sql.Clear;
             Sql.Add ('Select Kod_Lakoach,Sug_Rikuz,Kod_Atzmada From Lakoach');
             Sql.Add ('Where Kod_Lakoach = '+Trim(Copy(HStr,91,3)));
             Open;

             if FieldByName ('Kod_Lakoach').AsString = IntToStr(StrToInt(Trim(Copy(HStr,91,3)))) then
                begin
                Var_Sug_rikuz := FieldByName ('Sug_Rikuz').AsString;
                if (FieldByName ('Kod_Atzmada').AsInteger = 1) or
                   (FieldByName ('Kod_Atzmada').AsInteger = 2) then
                   DMod_Message.Tbl_AtmShib.FieldByname ('PriceKind').AsInteger:= 1{FieldByName ('Kod_Atzmada').AsInteger};
                end
             else
                Var_Sug_rikuz := '';
           end;

      PutMerakezDataInField(Var_Sug_Rikuz,DMod_Message.Tbl_AtmShib.FieldByname ('Mrakz'));
//-----------------------
(*
                        {����� �� �� ������}
      With DMod_Message.Qry_2 do
         begin
           Close;
           Sql.Clear;
           Sql.Add ('Select Code_Maslul,Name From Maslul');
           Sql.Add ('Where Code_Maslul = '+DMod_Message.Tbl_AtmShib.FieldByname ('MaslulCode1').AsString);
           Open;

           if FieldByName ('Code_Maslul').AsInteger=
              DMod_Message.Tbl_AtmShib.FieldByname ('MaslulCode1').AsInteger then
              DMod_Message.Tbl_AtmShib.FieldByname ('Maslul1').AsString:=
              FieldByName ('Name').AsString
           else
              ErrorLine(IntToStr(Mis_Shura),'���� ���� �� ����',Copy(HStr,41,10),Copy(HStr,131,9),Copy(HStr,11,7));
         end;  *)
      Except
//        ShowMessage ('���� ���� ���� ������� '+IntToStr (Mis_Shura));
        on e:Exception do
        ShowMessage (E.Message);
      end;
      try
        DMod_Message.Tbl_AtmShib.Post;
      except
        on e:Exception do
        begin
        ShowMessage (E.Message);

        ErrorLine(IntToStr(Mis_Shura),'���� ������ ������',Copy(HStr,50,6),Copy(HStr,74,5),Copy(HStr,60,7));
        end;
      end;

    end;  {end while}

  CloseFile (SonolFile);
  System.Close (PrintText);

  F_Mesage.Free;
  F_Mesage := nil;

  try
//    Dm.Tbl_Hotz_Temp.Post;
  except
  end;

  DMod_Message.Tbl_AtmAzmn.Close;
  DMod_Message.Tbl_AtmShib.Close;

  iF Sw_Stop then
     showmessage ('����� ������ ������ �"� ������')
  else
     showmessage ('������ ����� ������');
//----------------------- ���� ��"� ������
//  if Frm_Shagui = Nil then
//     Application.CreateForm(TFrm_Shagui, Frm_Shagui);

//  Frm_Shagui.QuickReport.Preview;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.SpeedButton1Click(Sender: TObject);
begin
  if Dmod_Message.Rehev_Search.Execute then
    begin
      Edt_Rehev.Text:= Dmod_Message.Rehev_Search.ReturnString;
    end; {end if}
end;
//----------------------------------------------------------------------
Procedure TF_Tenuot_Atarot.Bedikat_Netunim;
begin
  Sw_Takin := True;

  If (Edt_Shana.Text = '')  or
     (Edt_Month.Text = '') then
     begin
       ShowMessage ('��� ���� �/�� ��� ����');
       Sw_Takin := False;
       Exit;
     end;

  If Edt_Rehev.Text = '' then
     begin
       ShowMessage ('��� ��� ����');
       Sw_Takin := False;
       Exit;
     end;

  If Edt_Nehag.Text = '' then
     begin
       ShowMessage ('��� ��� ���');
       Sw_Takin := False;
       Exit;
     end;

  if not FileExists(Klita_Dir.Text) then
     begin
       ShowMessage ('������� �� ����� �/�� ����� �� ����');
       Sw_Takin := False;
       Exit;
     end;

   With DMod_Message.Qry_3 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Mis_Rishui From Rehev');
         Sql.Add ('Where Mis_Rishui = '+''''+Edt_Rehev.Text+'''');
         Open;

         if FieldByName ('Mis_Rishui').AsString <> Trim(Edt_Rehev.Text) then
            begin
              ShowMessage ('��� �� ���� ������');
              Sw_Takin := False;
              Exit;
            end;
       end;

   With DMod_Message.Qry_2 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Kod_Nehag From Nehag');
         Sql.Add ('Where Kod_Nehag = '+Edt_Nehag.Text);
         Open;

         if FieldByName ('Kod_Nehag').AsString <> Edt_Nehag.Text then
            begin
              ShowMessage ('��� �� ���� ������');
              Sw_Takin := False;
              Exit;
            end;
       end;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.SpeedButton2Click(Sender: TObject);
begin
  if Dmod_Message.Nehag_Search.Execute then
    begin
      Edt_Nehag.Text:= Dmod_Message.Nehag_Search.ReturnString;
    end; {end if}
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.Edt_NehagChange(Sender: TObject);
begin
   With DMod_Message.Qry_2 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Kod_Nehag,Shem_Nehag From Nehag');
         Sql.Add ('Where Kod_Nehag = '+Edt_Nehag.Text);
         Open;

         if FieldByName ('Kod_Nehag').AsString = Edt_Nehag.Text then
            begin
              Edt_Shem_nehag.Text := FieldByName ('Shem_Nehag').AsString;
            end
         else
            Edt_Shem_nehag.Text := '';
       end;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.FormShow(Sender: TObject);
var
  Year, Month, Day : Word;
begin
  DecodeDate(Date, Year, Month, Day); {����� ���� ������}
  Edt_Month.Text := IntToStr(Month);
  Edt_Shana.Text := IntToStr(Year);

  ReadIniFile;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.ReadIniFile;
begin
  IniFile := TIniFile.Create(DbIniFile);

  Edt_Nehag.Text := IniFile.ReadString ('Atarot', 'Kod_Nehag' ,'');
  Edt_rehev.Text := IniFile.ReadString ('Atarot', 'Mis_Rishui' ,'');
  Klita_Dir.Text := IniFile.ReadString ('Atarot', 'File_Name' ,'');

  IniFile.Free;

  Kod_Nehag := Edt_Nehag.Text;
  Mis_Rishui := Edt_Rehev.text;
  File_Name := Klita_Dir.Text;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.Bbn_SaveIniClick(Sender: TObject);
begin
  IniFile := TIniFile.Create(DbIniFile);

  IniFile.WriteString ('Atarot', 'Kod_Nehag' ,Edt_Nehag.Text);
  IniFile.WriteString ('Atarot', 'Mis_rishui' ,Edt_Rehev.Text);
  IniFile.WriteString ('Atarot', 'File_Name' ,Klita_Dir.Text);

  IniFile.Free;

  Kod_Nehag := Edt_Nehag.Text;
  Mis_Rishui := Edt_Rehev.text;
  File_Name := Klita_Dir.text;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.ErrorLine(Mis_Shura,Sug_Takala,Mis_Teuda,Nehag,Rehev : String);
begin
  Writeln (PrintText,
  Mis_Shura:5,'   ',
  Sug_Takala:25,'   ',
  Mis_Teuda:10,'   ',
  Nehag:10,'   ',
  Rehev:8,'   ');
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Atarot.BitBtn2Click(Sender: TObject);
begin
  Sw_Stop := True;
end;
//----------------------------------------------------------------------
Function ConvertToRishuy(InStr :String) :String;
Var
   StrH :String;
Begin
   StrH:= InStr;
   Case Length(StrH) of
        5:Result:=Copy(StrH,1,2)+'-'+Copy(StrH,3,3);
        6:Result:=Copy(StrH,1,3)+'-'+Copy(StrH,4,3);
        7:Result:=Copy(StrH,1,2)+'-'+Copy(StrH,3,3)+'-'+Copy(StrH,6,2);
   Else
       Result:=InStr;
   End;
end;

procedure TF_Tenuot_Atarot.SpeedButton3Click(Sender: TObject);
begin
(*
  DMod_Message.Sp_SonolException.ParamByName ('@TransferNo').AsString := 'Sonol';
  DMod_Message.Sp_SonolException.Active := Not DMod_Message.Sp_SonolException.Active;
  *)
//  DMod_Message.Sp_SonolException.Close;
end;

Function DelLeadingZeroFromRishuy (SrcStr:String):String;
var
  i:Integer;
  Tmp:String;

  Sw : Boolean;
begin
  Tmp:='';

  sw := False;
  For i:=1 To Length (SrcStr) Do
     if sw  then
        Tmp := Tmp+SrcStr[i]
     else
       begin
         if SrcStr[i] > '0' then
            begin
              Tmp := Tmp+SrcStr[i];
              sw := True;
            end
         else
            Tmp := Tmp+ ' ';
       end;
  Result := Tmp;
end;


function DosToWin(InStr:string):string;
var
  i : Integer;
begin
  for i := 1 To Length (InStr) Do
    if (Ord (InStr[i]) >= 128) And (Ord (InStr[i]) <=154 ) Then
       InStr[i] := Chr (Ord (InStr [i]) + 96)
    else
       InStr[i] := Chr (Ord (InStr [i]));
  DosToWin := InStr;
end;
//-----------------------------------------------------------------------
function LeftToRight(InStr:string):string;
var
   ix1,i:integer;
   HelpStr,Tmp,Tmp1:string;
begin
  HelpStr:='';
  Tmp := '';
  Tmp1:='';
  for ix1:= Length(InStr) downto 1 do
      HelpStr:=HelpStr+InStr[ix1];
  for ix1:= 1 to Length(HelpStr) Do
  begin
    if ((HelpStr[ix1] <'0') Or (HelpStr[ix1] >'9'))And (HelpStr[ix1]<>'.')And (HelpStr[ix1]<>',') Then
      begin
         For i:= Length (tmp1)downto 1  do
            Tmp:=Tmp+Tmp1[i];
         Tmp1:='';
         Tmp:=Tmp+HelpStr[ix1];
      end
    else
      Tmp1 := Tmp1+HelpStr[ix1]
  end;
  LeftToRight:=tmp;
end;

procedure TF_Tenuot_Atarot.PutMerakezDataInField(MerakezName:String;TheField :TField);
Var
   I,NumOfFields :LongInt;
   FieldName,StrH :String;
   FieldBuffer:String;
Begin
     FieldName:='';
     FieldBuffer:='';
     if Trim(MerakezName)='' Then Exit;
     NumOfFields:=CountFieldsInSepString(MerakezName,',');
     Try
        For I:=1 To NumOfFields Do
        Begin
           FieldName:=GetFieldFromSeparateString(MerakezName,',',I);
           StrH:=Dmod_Message.Tbl_AtmShib.FieldByName(FieldName).AsString;
           if Length(StrH)> 10 Then
              StrH:=Copy(StrH,1,10);
           While Length(StrH)<10 Do
           Begin
                 if Dmod_Message.Tbl_AtmShib.FieldByName(FieldName) is TNumericField Then
                    StrH:='0'+StrH
                 Else
                    StrH:=StrH+' ';
           End;
           FieldBuffer:=FieldBuffer+StrH;

        End;
        if Dmod_Message.Tbl_AtmShib.State=dsBrowse Then
           Dmod_Message.Tbl_AtmShib.Edit;
        TheField.AsString:=FieldBuffer;
     Except On e:Exception Do
          ShowMessage('�� ���� ������ ����'+
                      #13+E.Message);
     End;
End;

Function CountFieldsInSepString(TheString:String;TheSeparator :Char):LongInt;
// ���� ���� ������� ������ �"� �� �����
Var
   I :LongInt;
   NumOfSep :LongInt;
Begin
     if TheString='' Then
     Begin
          Result:=0;
          Exit;
     End;
     NumOfSep:=0;
     For I:=1 To Length(TheString) Do
     Begin
         if TheString[i]=TheSeparator Then
            Inc(NumOfSep);
     End;
     Result:=NumOfSep+1;
End;

//         ������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                         FieldNum:LongInt): String;
Var
   StrH :String;
   Ix1,FieldCount,FieldPos :LongInt;

Begin
    if TheString='' Then
    Begin
        Result:='';
        Exit;
    End;
    if (FieldNum=1) Then
    Begin
        if TheString[1]=Separator Then
             Result:=''
        Else
        Begin
             StrH:=Copy(TheString,1,Pos(Separator,TheString)-1);
             if StrH='' Then StrH:=TheString;
             {EndOfString:=Copy(TheString,Pos(Separator,TheString)+1,Length(TheString)-Length(StrH)+1);}
             Result:=StrH;
        End;
    End
    Else {FieldNum>1}
    Begin
         FieldCount:=1;
         For Ix1:=1 To Length(TheString) Do
            if TheString[Ix1]=Separator Then
            Begin
                 FieldPos:=Ix1+1;
                 FieldCount:=FieldCount+1;
                 if FieldCount=FieldNum Then Break;
            End;
         if (FieldCount=FieldNum) And (FieldPos<=Length(TheString)) Then
         Begin
             StrH:=Copy(TheString,FieldPos,Length(TheString)-FieldPos+1);
             if Pos(Separator,StrH)>0 Then
                StrH:=Copy(StrH,1,Pos(Separator,StrH)-1);
             Result:=StrH;
         End
         Else
             Result:='';
    End;
End;

procedure Call_Tenuot_Atarot(LDbAliasName,LDbUserName,LDbPassword,LIniFile:Pchar);
begin
  DbAliasName := LDbAliasName;
  DbUserName := LDbUserName;
  DbPassword := LDbPassword;
  DbIniFile := LIniFile;
  
  if Dmod_Message = Nil Then
     Dmod_Message := TDmod_Message.Create (Nil);

  if F_Tenuot_Atarot = Nil then
     F_Tenuot_Atarot := TF_Tenuot_Atarot.Create (Nil);
  F_Tenuot_Atarot.ShowModal;
//----------------
  If Dmod_Message <> nil then
   begin
     Dmod_Message.Free;
     Dmod_Message := Nil;
   end;

  If F_Tenuot_Atarot <> nil then
   begin
     F_Tenuot_Atarot.Free;
     F_Tenuot_Atarot := Nil;
   end;

end;


end.

